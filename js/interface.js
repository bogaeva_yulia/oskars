$(function(){
    $('#audio-player').mediaelementplayer({
        alwaysShowControls: true,
        features: ['playpause','progress','current'],
        audioVolume: 'horizontal',
        audioWidth: 450,
        audioHeight: 140,
        iPadUseNativeControls: false,
        iPhoneUseNativeControls: false,
        AndroidUseNativeControls: false
    });
});

$(document).ready(function () {
    var myPlayer = videojs('video', {
        autoplay: false,
        loop: false,
        controlBar: {
            bigPlayButton: true,
            muteToggle: false,
            playToggle: true,
            timeDivider: true,
            currentTimeDisplay: true,
            durationDisplay: true,
            remainingTimeDisplay: false,
            progressControl: true,
            fullscreenToggle: false,
            volumeControl: false,
            nativeControlsForTouch: false
        }
    });
});

